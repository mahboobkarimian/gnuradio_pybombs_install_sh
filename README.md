# FULL DESCRIPTION:
This is a bash shell script to easily install GNUradio, UHD and many other applications and libraries to use in GNUradio.
First of all, it installs the various required dependencies, then according to recipes, it will install GNUradio and others.
This will create a directory called "prefix/stable" in your home directory, and install packages to them.

# ADD ADDITIONAL PACKAGES:
You can easily add your packages to install if you need them.
For example, add these lines to the end of the install.sh file and save:
```
    pybombs install limesuite
    pybombs install gr-limesdr
```
OR, you can install them later (After installtion):
```
    cd ~/prefix/stable
    source ./setup_env.sh
    pybombs install limesuite
    pybombs install gr-limesdr
```

# RUN the installer:
Download the .sh file, copy it to your "Home" directory and set the permissions to run by chmod command.
and then run it by:
`    ./install.sh`

# RUN the applications from command line:
```
    cd ~/prefix/stable
    source ./setup_env.sh
```

and then, for example, type:
`    gnuradio-companion`

* Please note that based on some experiments, in old CPUs, GNUradio gives you some unsolvable errors if you are using specific libraries for building your own blocks:
```
Warning: restarting the docstring loader (crashed while loading XYZ)
Warning: docstring loader crashed too often
Illegal instruction (core dumped)
or
Segmentation fault (core dumped)
```
* They are related to python.
