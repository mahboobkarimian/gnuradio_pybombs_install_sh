#!/bin/bash
sudo apt-get -y install git python-setuptools python-dev python-pip build-essential gtk+-3.0 libliquid1d libliquid-dev
sudo apt-get -y install cmake g++ libpython-dev python-numpy swig python-wheel libcppunit-dev python-cheetah python-qt4
sudo apt-get -y install libcanberra-gtk-module libcanberra-gtk3-module
sudo -H pip install --upgrade pip
sudo -H pip install git+https://github.com/gnuradio/pybombs.git
sudo -H pip install pyqtgraph
pybombs recipes add gr-recipes git+https://github.com/gnuradio/gr-recipes.git
pybombs recipes add gr-etcetera git+https://github.com/gnuradio/gr-etcetera.git
pybombs recipes add ettus git+https://github.com/EttusResearch/ettus-pybombs.git

pybombs config makewidth 4
cd
mkdir -p prefix/stable/src
cd ~/prefix/stable/src
git clone --recursive git://github.com/EttusResearch/uhd.git
cd
pybombs prefix init prefix/stable -a stable -R gnuradio-stable
cd ~/prefix/stable
source ./setup_env.sh
cd
pybombs install gqrx